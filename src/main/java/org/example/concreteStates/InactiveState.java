package org.example.concreteStates;

import org.example.context.StudentContext;
import org.example.interfaces.StudentState;

public class InactiveState implements StudentState {
    @Override
    public void performAction(StudentContext context) {
        System.out.println("Student is in Inactive State");
        // Logic khi sinh viên ở trạng thái không hoạt động
    }
}

