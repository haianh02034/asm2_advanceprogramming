package org.example.concreteStates;

import org.example.context.StudentContext;
import org.example.interfaces.StudentState;

public class ActiveState implements StudentState {
    @Override
    public void performAction(StudentContext context) {
        System.out.println("Student is in Active State");
        // Logic khi sinh viên ở trạng thái hoạt động
    }
}


