// StudentServiceProxy.java
package org.example.proxy;

import org.example.interfaces.StudentServiceInterface;
import org.example.service.RealStudentService;

public class StudentServiceProxy implements StudentServiceInterface {
    private RealStudentService realStudentService;

    @Override
    public void requestStudentInfo() {
        if (realStudentService == null) {
            realStudentService = new RealStudentService();
        }
        // Additional logic (e.g., access control, caching, logging) can be added here
        System.out.println("Proxy: Requesting student information...");
        realStudentService.requestStudentInfo();
    }
}
