// StudentServiceInterface.java
package org.example.interfaces;

public interface StudentServiceInterface {
    public void requestStudentInfo();
}
