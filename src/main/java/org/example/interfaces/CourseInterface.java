package org.example.interfaces;

import org.example.entity.Course;

import java.util.List;

public interface CourseInterface {
    Course addCourse(Course course);
    Course updateCourse(Course course);
    boolean deleteCourse(String courseID);
    List<Course> getAllCourses();
    Course getCourseById(String courseID);
}
