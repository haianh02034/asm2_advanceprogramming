package org.example.interfaces;

import org.example.context.StudentContext;

public interface StudentState {

        void performAction(StudentContext context);


}
