package org.example;

import org.example.builder.StudentBuilder;
import org.example.context.StudentContext;
import org.example.decorators.MilkDecorator;
import org.example.proxy.StudentServiceProxy;
import org.example.entity.Student;
import org.example.entity.Teacher;
import org.example.interfaces.NotifyInterface;
import org.example.interfaces.StudentServiceInterface;
import org.example.interfaces.UpdateInfoRequest;
import org.example.service.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//Singleton-Structural
        Teacher teacher = new Teacher(1,"name");
        TeacherService.getInstance().addTeacher(teacher);
        System.out.println("Hello world!");

//Iterator
        Student student1 = new Student(1,"Anh");
        Student student2 = new Student(2,"Ba");
        Student student3 = new Student(3,"Co");
        List<Student> studentList = new ArrayList<>();

        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        Iterator<Student> iterator = studentList.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            System.out.println(student.toString());
        }
//Adapter
        UpdateInfoRequest updateInfoRequest = new AdminService(TeacherService.getInstance());
        updateInfoRequest.updateInfoTeacher(teacher);

// Factory Method == Creational Pattern
        NotifyInterface notifyStudent = AccountFactory.createNotification("Student");
        notifyStudent.notification();

        NotifyInterface notifyTeacher = AccountFactory.createNotification("Teacher");
        notifyTeacher.notification();

        NotifyInterface notifyParent = AccountFactory.createNotification("Parent");
        notifyParent.notification();

//Facade == Structural Pattern

        FacadeService facadeService = new FacadeService();
        facadeService.operationDeleteTeacher("name");
//Builder Pattern

        Student student = new StudentBuilder("Student@7ba4f24f")
                .setAccountBalance(123)
                .setEnrollmentDate("29-11-2003")
                .build();
        System.out.println(student.toString());

//State Pattern
        StudentContext studentContext = new StudentContext();

        studentContext.performAction(); // Xuất ra
    }

//Proxy Pattern
//    StudentServiceInterface studentService = new StudentServiceProxy();
//    studentService.requestStudentInfo();


    StudentServiceInterface studentService = new StudentServiceProxy();
    StudentServiceInterface milkDecoratedService = new MilkDecorator(studentService);

        milkDecoratedService.requestStudentInfo();
}