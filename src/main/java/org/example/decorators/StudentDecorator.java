package org.example.decorators;


import org.example.interfaces.StudentServiceInterface;

public interface StudentDecorator extends StudentServiceInterface {
    // Additional methods can be added here
}

