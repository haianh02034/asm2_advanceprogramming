package org.example.decorators;

import org.example.interfaces.StudentServiceInterface;

public class MilkDecorator implements StudentDecorator {
    private StudentServiceInterface decoratedStudentService;

    public MilkDecorator(StudentServiceInterface decoratedStudentService) {
        this.decoratedStudentService = decoratedStudentService;
    }

    @Override
    public void requestStudentInfo() {
        // Additional behavior before the original request
        System.out.println("Adding milk to student information...");

        // Delegate the request to the decorated object
        decoratedStudentService.requestStudentInfo();

        // Additional behavior after the original request
        System.out.println("Milk added to student information.");
    }
}
