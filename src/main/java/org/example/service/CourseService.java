package org.example.service;

import org.example.entity.Course;
import org.example.interfaces.CourseInterface;

import java.util.List;

public class CourseService implements CourseInterface {

    // Singleton - Design Pattern
    public static CourseService instance = null;
    public static synchronized CourseService getInstance()
    {
        if(instance == null){
            instance = new CourseService();
        }
        return instance;
    }

    @Override
    public Course addCourse(Course course) {
        return null;
    }

    @Override
    public Course updateCourse(Course course) {
        return null;
    }

    @Override
    public boolean deleteCourse(String courseID) {
        return false;
    }

    @Override
    public List<Course> getAllCourses() {
        return null;
    }

    @Override
    public Course getCourseById(String courseID) {
        return null;
    }

    public void deleteByTeacherID(String teacherID) {
    }
}
