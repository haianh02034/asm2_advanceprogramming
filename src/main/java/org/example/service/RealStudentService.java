// RealStudentService.java
package org.example.service;

import org.example.interfaces.StudentServiceInterface;

public class RealStudentService implements StudentServiceInterface {
    @Override
    public void requestStudentInfo() {
        System.out.println("Fetching real student information...");
    }
}
