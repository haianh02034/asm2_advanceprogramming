package org.example.entity;
import java.util.Date;
import java.util.List;
public class Course {
    private String courseID;
    private String name;
    private String subject;
    private Teacher teacher;
    private String lecture;
    private List<Student> students;
    public Course(){
    }
    public Course(String courseID, String name, String subject,
                  Teacher teacher, String lecture, List<Student> students)
    {
        this.courseID = courseID;
        this.name = name;
        this.subject = subject;
        this.teacher = teacher;
        this.lecture = lecture;
        this.students = students;
    }
    public String getCourseID() {
        return courseID;
    }
    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public Teacher getTeacher() {
        return teacher;
    }
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
    public String getLecture() {
        return lecture;
    }
    public void setLecture(String lecture) {
        this.lecture = lecture;
    }
    public List<Student> getStudents() {
        return students;
    }
    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
