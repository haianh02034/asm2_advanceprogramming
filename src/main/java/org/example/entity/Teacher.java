package org.example.entity;

import org.example.core.Person;
import org.example.interfaces.NotifyInterface;

public class Teacher extends Person implements NotifyInterface {
    private String teacherID;
    public Teacher() {

    }

    public Teacher(String userID, String password, boolean loginStatus, String name, String address, String phoneNumber, String gender, String identifiCard, String teacherID) {
        super(userID, password, loginStatus, name, address, phoneNumber, gender, identifiCard);
        this.teacherID = teacherID;
    }

    public Teacher(int i, String name) {
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    @Override
    public void notification() {
        System.out.println("Teacher notify");
    }
}
