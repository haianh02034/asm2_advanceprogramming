package org.example.builder;

import org.example.entity.Student;

public class StudentBuilder {
    public double accountBalance;
    public String studentID;
    public String enrollmentDate;
    // Các thuộc tính khác


    public StudentBuilder (String studentID) {
        this.studentID = studentID;

    }

    public StudentBuilder setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
        return this;
    }

    public StudentBuilder setEnrollmentDate(String enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
        return this;
    }

    // Các phương thức setter cho các thuộc tính khác

    public Student build() {
        return new Student(this);
    }
}
