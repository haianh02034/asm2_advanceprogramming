package org.example.context;

import org.example.concreteStates.ActiveState;
import org.example.interfaces.StudentState;

public class StudentContext {
    private StudentState currentState;

    public StudentContext() {
        currentState = new ActiveState();
    }

    public void setState(StudentState state) {
        currentState = state;
    }

    public void performAction() {
        currentState.performAction(this);
    }
}

